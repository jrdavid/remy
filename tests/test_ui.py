import sys
import pytest
from remy.ui import BatchUI, EmptyPrompt

def _raise(ex):
    raise ex


class TestBatchUI:
    def test_read_prompt(self, mocker):
        mocker.patch.object(sys, "stdin", read=lambda: "test prompt\n")
        ui = BatchUI()
        assert ui.read_prompt() == "test prompt"

    def test_read_prompt_empty(self, mocker):
        mocker.patch.object(sys, "stdin", read=lambda: "\n")
        ui = BatchUI()
        with pytest.raises(EmptyPrompt):
            ui.read_prompt()

    def test_write_response(self, mocker):
        mock_write = mocker.patch.object(sys.stdout, "write")
        ui = BatchUI()
        ui.write_response("test response")
        mock_write.assert_called_once_with("test response")

    def test_read_prompt_keyboard_interrupt(self, mocker):
        mocker.patch.object(sys, "stdin", read=lambda: _raise(KeyboardInterrupt))
        ui = BatchUI()
        with pytest.raises(EmptyPrompt):
            ui.read_prompt()
