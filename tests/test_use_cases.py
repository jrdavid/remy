import pytest
from remy.use_cases import Completion, EmptyPrompt


@pytest.fixture
def mock_model(mocker):
    return mocker.Mock()


@pytest.fixture
def mock_ui(mocker):
    return mocker.Mock()


@pytest.fixture
def completion(mock_model, mock_ui):
    return Completion(mock_model, mock_ui)


class TestCompletion:
    def test_run_normal(self, mock_ui, mock_model, completion):
        # Arrange
        mock_ui.read_prompt.return_value = "test prompt"
        mock_model.complete.return_value = "test response"

        # Act
        completion.run()

        # Assert
        mock_ui.read_prompt.assert_called_once()
        mock_model.complete.assert_called_once_with("test prompt")
        mock_ui.write_response.assert_called_once_with("test response")

    def test_run_empty_prompt(self, mock_ui, mock_model, completion):
        # Arrange
        mock_ui.read_prompt.side_effect = EmptyPrompt

        # Act
        completion.run()

        # Assert
        mock_ui.read_prompt.assert_called_once()
        mock_ui.write_response.assert_called_once_with("Empty prompt. Goodbye!")
