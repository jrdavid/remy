import pytest
from remy.models import OpenAIModel

@pytest.fixture
def mock_openai(mocker):
    mock = mocker.patch('remy.models.OpenAI')
    mock().chat.completions.create.return_value = mocker.Mock(choices=[mocker.Mock(message=mocker.Mock(content='test content'))])
    return mock

@pytest.fixture
def model():
    return OpenAIModel()

class TestOpenAIModel:
    def test_complete(self, mocker):
        # Arrange
        mock_openai = mocker.patch('remy.models.OpenAI')
        mock_openai().chat.completions.create.return_value = mocker.Mock(choices=[mocker.Mock(message=mocker.Mock(content='test content'))])
        model = OpenAIModel()

        # Act
        result = model.complete('test prompt')

        # Assert
        assert result == 'test content'
        mock_openai().chat.completions.create.assert_called_once_with(
            model="gpt-3.5-turbo",
            messages=[
                {"role": "system", "content": "You are a helpful assistant."},
                {"role": "user", "content": 'test prompt'},
            ],
        )
