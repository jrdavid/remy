class EmptyPrompt(Exception):
    pass


class Completion:
    def __init__(self, model, ui):
        self.model = model
        self.ui = ui

    def run(self):
        try:
            prompt = self.ui.read_prompt()
            response = self.model.complete(prompt)
            self.ui.write_response(response)
        except EmptyPrompt:
            self.ui.write_response("Empty prompt. Goodbye!")
