import sys

from remy.use_cases import EmptyPrompt


class BatchUI:
    def __init__(self):
        pass

    def read_prompt(self):
        try:
            prompt = sys.stdin.read().strip()
            if not prompt:
                raise EmptyPrompt()
            return prompt
        except KeyboardInterrupt:
            raise EmptyPrompt()

    def write_response(self, response):
        sys.stdout.write(response)
