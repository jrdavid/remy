from openai import OpenAI


class OpenAIModel:
    def __init__(self):
        self.client = OpenAI()

    def complete(self, prompt):
        completions = self.client.chat.completions.create(
            model="gpt-3.5-turbo",
            messages=[
                {"role": "system", "content": "You are a helpful assistant."},
                {"role": "user", "content": prompt},
            ],
        )
        return completions.choices[0].message.content
