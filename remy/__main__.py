import remy.models as models
import remy.use_cases as use_cases
import remy.ui as ui


def main():
    user_interface = ui.BatchUI()
    model = models.OpenAIModel()
    use_case = use_cases.Completion(model, user_interface)
    use_case.run()


if __name__ == "__main__":
    main()
